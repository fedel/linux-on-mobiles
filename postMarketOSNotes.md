# General info to use postmarketos

## References
* https://wiki.postmarketos.org/wiki/Installation_guide
* https://wiki.postmarketos.org/wiki/WiFi#Using_NetworkManager

## Steps
* Install pmbootstrap 
`pip3 install pmbootstrap --user`

* `pmbootstrap init` - Then follow the steps
* `pmbootstrap install`
* `pmbootstrap flasher flash_rootfs`
* `pmbootstrap flasher flash_kernel`

## To change the chroot before install:
`pmbootstrap chroot -r`


