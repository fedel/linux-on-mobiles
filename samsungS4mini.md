# Samsung S4 Mini i9195

Tests using PostMarketOS

## References

https://wiki.postmarketos.org/wiki/Samsung_Galaxy_S4_Mini_LTE_(samsung-i9195)

## General issues

* To enable internet through usb
`sudo ip link set dev enp0s20f0u3 address 06:4b:2d:06:4b:2d`

* ssh didn't work - I've tried somethings but nothing worked

`packet_write_wait: Connection to 192.168.1.103 port 22: Broken pipe`

* Cannot shutdown (it start by itself after shutdown)

* Cannot boot if is connected to the power or if is connected to a computer 

## xfce4 issues

* After some time without use the mouse click stops work (the same error doesn't happen using mate)

## How to connect to it

* Install postmarket os with the matchbox-keyboard to be able to run things from it
* Execute telnetd:
`telnetd -F`
* Then is possible to connect to it using telnet
* It is possible to connect the mobile to the wifi using :

```
# check all available wifi
sudo nmcli device wifi list ifname wlan0 
sudo nmcli device wifi connect $WIFIID password $WIFIPASSW ifname wlan0
# 
```

### A more permanente solution making wifi autoconnect and telnetd auto start

* Create files on /etc/local.d with the commands to start
* ConnectWifi.start and Telnet.start with exection permission (chmod +x)
* Using the same comands showed above
* The files on this folder are executed on lexical order


## Rotate display

https://wiki.postmarketos.org/wiki/Display

## Next steps

* Fix ssh
* Try kernel mainline
* Test armv7 - https://wiki.postmarketos.org/wiki/Tips_and_tricks#Update_the_architecture_of_your_device 
    * Check if is not using it already
